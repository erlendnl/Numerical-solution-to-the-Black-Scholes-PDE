# Black-Scholes
Numerical solution to the Black-Scholes PDE using finite difference methods FDM.

PDE:
```math
u_t - \frac{1}{2}\sigma^2 x^2 u_{xx} - rxu_x + cu &= 0
```
